package workshops.lambdas_streams.uebung_8_reducestream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Loesung_8_ReduceStream {
    public static void main(String[] args) {
        new Loesung_8_ReduceStream().run();
    }

    private void run() {
        String[] participantArray = {"Uwe", "Monica", "Lisa", "Gergely", "Mathias", "Dennis", "Maja", "Julia", "Umut",
                "Thomas", "Irina", "Matthias", "Georg", "Osterhase", "Guillaume", "Thomas", "Fabio", "Mareike", "Nga",
                "Mona", "Nikolaus", "Torsten", "Ingo", "Maher", "Weihnachtsmann"};

        Stream<String> realParticipantsStream = Arrays.stream(participantArray)
                .filter(this::isRealPerson)
                .filter(name -> !name.equals("Torsten"));

        List<String> teamList = new ArrayList<>();

        realParticipantsStream
                .reduce((name1, name2) -> {
                    teamList.add(name1 + " / " + name2);
                    return name2;
                });
        teamList.forEach(p -> System.out.println(p));
    }

    private boolean isRealPerson(String name) {
        if (name.equals("Osterhase") || name.equals("Nikolaus") || name.equals("Weihnachtsmann")) {
            return false;
        }
        return true;
    }
}
