package workshops.lambdas_streams.uebung_8_reducestream;

import java.util.Arrays;
import java.util.stream.Stream;

public class Aufgabe_8_ReduceStream {
    public static void main(String[] args) {
        new Aufgabe_8_ReduceStream().run();
    }

    private void run() {
        String[] participantArray = {"Uwe", "Monica", "Lisa", "Gergely", "Mathias", "Dennis", "Maja", "Julia", "Umut",
                "Thomas", "Irina", "Matthias", "Georg", "Osterhase", "Guillaume", "Thomas", "Fabio", "Mareike", "Nga",
                "Mona", "Nikolaus", "Torsten", "Ingo", "Maher", "Weihnachtsmann"};

        Stream<String> realParticipantsStream = Arrays.stream(participantArray)
                .filter(this::isRealPerson)
                .filter(name -> !name.equals("Torsten"));

        // Aufgabe 8: Bilde aus den Teilnehmern Gruppen zu je 2 Personen und gib sie auf der Console aus:
        //            Uwe/Monica
        //            Lisa/Gergely
        //            usw...
        //
        //            Hinweis: Nutze dafür die reduce()-Methode der Klasse Stream und baue eine neue Liste auf.
    }

    private boolean isRealPerson(String name) {
        if (name.equals("Osterhase") || name.equals("Nikolaus") || name.equals("Weihnachtsmann")) {
            return false;
        }
        return true;
    }
}
