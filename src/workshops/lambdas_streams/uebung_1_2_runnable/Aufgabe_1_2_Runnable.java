package workshops.lambdas_streams.uebung_1_2_runnable;

public class Aufgabe_1_2_Runnable {
    public static void main(String[] args) {
        new Aufgabe_1_2_Runnable().tuWas();
    }

    private void tuWas() {

        Runnable evenPrinter = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <= 100; i += 2) {
                    System.out.println(i);
                }
            }
        };

        Runnable oddPrinter = new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i <= 99; i += 2) {
                    System.out.println(i);
                }
            }
        };

        starteAktionen(evenPrinter, oddPrinter);
    }

    private void starteAktionen(Runnable aktion1, Runnable aktion2) {
        aktion1.run();
        aktion2.run();
    }

    // Aufgabe 1: Verwende für evenPrinter und oddPrinter lambdas anstatt anonymer innerer Klassen.

    // Aufgabe 2: Anstatt zwei fast gleiche lambdas zu erstellen, schreibe eine Methode mit folgender Signatur:
    //                private Runnable createLoopPrinter(int start, int stop, int inkrement) {
    //            Diese Methode wird dann zweimal mit unterschiedlichen Parametern aufgerufen, um die passenden Runnalbes zu erzeugen.
}
