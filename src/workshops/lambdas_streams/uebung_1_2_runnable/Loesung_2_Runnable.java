package workshops.lambdas_streams.uebung_1_2_runnable;

public class Loesung_2_Runnable {
    public static void main(String[] args) {
        new Loesung_2_Runnable().tuWas();
    }

    private void tuWas() {
        starteAktionen(
                createLoopPrinter(0, 100, 2),
                createLoopPrinter(1, 99, 2)
        );
    }

    private Runnable createLoopPrinter(int start, int stop, int inkrement) {
        Runnable loopPrinter = () -> {
            for (int i = start; i <= stop; i += inkrement) {
                System.out.println(i);
            }
        };
        return loopPrinter;
    }

    private void starteAktionen(Runnable aktion1, Runnable aktion2) {
        aktion1.run();
        aktion2.run();
    }
}
