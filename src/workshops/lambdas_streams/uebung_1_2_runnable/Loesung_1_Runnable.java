package workshops.lambdas_streams.uebung_1_2_runnable;

public class Loesung_1_Runnable {
    public static void main(String[] args) { new Loesung_1_Runnable().tuWas(); }

    private void tuWas() {
        Runnable evenPrinter = () -> {
            for (int i = 0; i <= 100; i += 2) {
                System.out.println(i);
            }
        };

        Runnable oddPrinter = () -> {
            for (int i = 1; i <= 99; i += 2) {
                System.out.println(i);
            }
        };

        starteAktionen(evenPrinter, oddPrinter);

    }

    private void starteAktionen(Runnable aktion1, Runnable aktion2) {
        aktion1.run();
        aktion2.run();
    }
}

