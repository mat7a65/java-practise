package workshops.lambdas_streams.uebung_7_earlybirds;

import java.util.Arrays;
import java.util.stream.Stream;

public class Aufgabe_7_EarlyBirds {
    public static void main(String[] args) {
        new Aufgabe_7_EarlyBirds().run();
    }

    private void run() {
        // Teilnehmer in Anmeldereihenfolge
        String[] participantArray = {"Uwe", "Monica", "Lisa", "Gergely", "Mathias", "Dennis", "Maja", "Julia", "Umut",
                "Thomas", "Irina", "Matthias", "Georg", "Osterhase", "Guillaume", "Thomas", "Fabio", "Mareike", "Nga",
                "Mona", "Nikolaus", "Torsten", "Ingo", "Maher", "Weihnachtsmann"};

        Stream<String> realParticipantsStream = Arrays.stream(participantArray)
                .filter(this::isRealPerson)
                .filter(name -> !name.equals("Torsten"));

        // Aufgabe7: Hole die ersten 5 Namen aus dem Stream heraus und erzeuge daraus ein neues Array (5 Teilnehmer, die sich als erste angemeldet haben).
        //           Hinweis: Nutze die passenden Methoden der Klasse Stream: Recherchiere dazu im JavaDoc von Stream.
        //           Anschließend sollen die Namen der 5 EarlyBirds möglichst einfach ausgegeben werden
        // String[] earlyBirds = ...;
    }

    private boolean isRealPerson(String name) {
        if (name.equals("Osterhase") || name.equals("Nikolaus") || name.equals("Weihnachtsmann")) {
            return false;
        }
        return true;
    }
}
