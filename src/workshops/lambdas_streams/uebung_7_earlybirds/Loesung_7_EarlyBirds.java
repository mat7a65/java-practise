package workshops.lambdas_streams.uebung_7_earlybirds;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Loesung_7_EarlyBirds {
    public static void main(String[] args) {
        new Loesung_7_EarlyBirds().run();
    }

    private void run() {
        // Teilnehmer in Anmeldereihenfolge
        String[] participantArray = {"Uwe", "Monica", "Lisa", "Gergely", "Mathias", "Dennis", "Maja", "Julia", "Umut",
                "Thomas", "Irina", "Matthias", "Georg", "Osterhase", "Guillaume", "Thomas", "Fabio", "Mareike", "Nga",
                "Mona", "Nikolaus", "Torsten", "Ingo", "Maher", "Weihnachtsmann"};

        List<String> earlyBirds = new ArrayList<>();

//        Stream<String> realParticipantsStream =
        Arrays.stream(participantArray)
                .filter(name1 -> isRealPerson(name1))
                .filter(name -> !name.equals("Torsten"))
                .limit(5)
                .forEach(p -> System.out.println(p));
    }

    private boolean isRealPerson(String name) {
        if (name.equals("Osterhase") || name.equals("Nikolaus") || name.equals("Weihnachtsmann")) {
            return false;
        }
        return true;
    }
}
