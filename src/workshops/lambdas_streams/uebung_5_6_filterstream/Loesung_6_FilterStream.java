package workshops.lambdas_streams.uebung_5_6_filterstream;

import java.util.Arrays;

public class Loesung_6_FilterStream {
    public static void main(String[] args) {new Loesung_6_FilterStream().run();}

    private void run() {
        String[] participantArray = {"Uwe", "Monica", "Lisa", "Gergely", "Mathias", "Dennis", "Maja", "Julia", "Umut",
                "Thomas", "Irina", "Matthias", "Georg", "Osterhase", "Guillaume", "Thomas", "Fabio", "Mareike", "Nga",
                "Mona", "Nikolaus", "Torsten", "Ingo", "Maher", "Weihnachtsmann"};

        Arrays.stream(participantArray)
                .filter(this::isRealPerson)
                .filter(p -> !p.equals("Torsten"))
                .mapToInt(p -> p.length())
                .forEach(System.out::println);
    }

    private boolean isRealPerson(String name) {
        if (name.equals("Osterhase") || name.equals("Nikolaus") || name.equals("Weihnachtsmann")) {
            return false;
        }
        return true;
    }
}
