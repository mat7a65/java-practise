package workshops.lambdas_streams.uebung_5_6_filterstream;

import java.util.Arrays;

public class Aufgabe_5_6_FilterStream {
    public static void main(String[] args) {
        new Aufgabe_5_6_FilterStream().run();
    }

    private void run() {
        String[] participantArray = {"Uwe", "Monica", "Lisa", "Gergely", "Mathias", "Dennis", "Maja", "Julia", "Umut",
                "Thomas", "Irina", "Matthias", "Georg", "Osterhase", "Guillaume", "Thomas", "Fabio", "Mareike", "Nga",
                "Mona", "Nikolaus", "Torsten", "Ingo", "Maher", "Weihnachtsmann"};

        Arrays.stream(participantArray).filter(this::isRealPerson).forEach(System.out::println);
    }

    private boolean isRealPerson(String name) {
        if (name.equals("Osterhase") || name.equals("Nikolaus") || name.equals("Weihnachtsmann")) {
            return false;
        }
        return true;
    }

    // Aufgabe 5: Der Trainer hat sich unter die Teilnehmer gemogelt.
    //            Füge einen weiteren Filter-Schritt hinzu, um den Trainer aus
    //            dem Teilnehmer-Stream herauszuschmeißen.
    //            Hinweis: Die Methode isRealPerson() soll NICHT verändert werden;
    //            der Trainer ist schließlich eine reale Person ;-)
    //                     Es soll ein zweiter Filter-Schritt hinzugefügt werden.
    //                     Erzeuge auch KEINE neue Methode, sondern verwende
    //                     zur Abwechslung mal wieder einen direkten Lambda-Ausdruck
    //
    // Aufgabe 6: Gib anstelle der Namen der Teilnehmer, jeweils die
    // Länge der einzelnen Namen in Zeichen aus.
    //            Hinweis: Verwende die Methode mapToInt() der Klasse Stream.
}
