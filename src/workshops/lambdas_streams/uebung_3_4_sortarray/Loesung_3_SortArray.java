package workshops.lambdas_streams.uebung_3_4_sortarray;

import java.util.Arrays;
import java.util.Comparator;

public class Loesung_3_SortArray {
    public static void main(String[] args) {
        new Loesung_3_SortArray().run();
    }

    private void run() {
        String[] words = {"Herzlich", "Willkommen", "zum", "Workshop", "für", "Lambdas", "und", "Streams", "!"};

        Arrays.sort(words, comparator);

        for (String word : words) {
            System.out.print(word + " ");
        }
        System.out.println();
    }


    private Comparator<String> comparator = (o1, o2) -> {
        if (o1.length() < o2.length()) {
            return -1;
        } else if (o1.length() > o2.length()) {
            return 1;
        }
        return 0;
    };
}
