package workshops.lambdas_streams.uebung_3_4_sortarray;

import java.util.Arrays;
import java.util.Comparator;

public class Aufgabe_3_4_SortArray {
    public static void main(String[] args) {
        new Aufgabe_3_4_SortArray().run();
    }

    private void run() {
        String[] words = {"Herzlich", "Willkommen", "zum", "Workshop", "für", "Lambdas", "und", "Streams", "!"};

        Arrays.sort(words, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1.length() < o2.length()) {
                    return -1;
                } else if (o1.length() > o2.length()) {
                    return 1;
                }
                return 0;
            }
        });

        for (String word : words) {
            System.out.print(word + " ");
        }
        System.out.println();
    }

    // Aufgabe 3: Im Beispiel bekommt Arrays.sort() einen Comparator übergeben, der durch eine anonyme innere Klasse
    // erzeugt wird.
    // Schreibe stattdessen eine Comparator-Methode als Instanz-Methode und übergib eine Methoden-Referenz
    // auf diese Methode an Arrays.sort().

    // Aufgabe 4: Finde eine Möglichkeit, die for-Schleife zur Ausgabe aller Worte in einer Zeile zu schreiben.
    //            Tipp: Die Zeile beginnt mit: Arrays.asList(words).forEach(
}

