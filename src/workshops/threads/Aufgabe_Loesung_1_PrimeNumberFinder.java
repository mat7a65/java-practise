package workshops.threads;

public class Aufgabe_Loesung_1_PrimeNumberFinder {

    private static final int SIZE = 10_000;
    private static int counter = 0;

    private boolean[] primeNumbers;


    public static void main(String[] args) throws InterruptedException {
        new Aufgabe_Loesung_1_PrimeNumberFinder().findPrimeNumbers();
    }

    public Aufgabe_Loesung_1_PrimeNumberFinder() {
        this.primeNumbers = new boolean[SIZE];
    }

//    private void findPrimeNumbers() {
//        for (int i = 0; i < SIZE; i++) {
//            primeNumbers[i] = isPrime(i);
//            if (primeNumbers[i]) {
//                System.out.println(i + " ist eine Primzahl!");
//            }
//        }
//    }

    private void findPrimeNumbers() throws InterruptedException {

        Runnable firstHalfR = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < SIZE / 2; i++) {
                    primeNumbers[i] = isPrime(i);
                    if (primeNumbers[i]) {
                        System.out.println(i + " ist eine Primzahl");
                        counter++;
                    }
                }
            }
        };

        Runnable secondHalfR = new Runnable() {
            @Override
            public void run() {
                for (int i = SIZE - 1; i >= SIZE / 2; i--) {
                    primeNumbers[i] = isPrime(i);
                    if (primeNumbers[i]) {
                        System.out.println(i + " ist eine Primzahl");
                        counter++;
                    }
                }
            }
        };

//        Thread firstHalfT = new Thread(() -> {
//            for (int i = 0; i < SIZE / 2; i++) {
//                primeNumbers[i] = isPrime(i);
//                if (primeNumbers[i]) {
//                    System.out.println(i + " ist eine Primzahl");
//                }
//            }
//        });
//
//        Thread secondHalfT = new Thread(() -> {
//            for (int i = SIZE - 1; i >= SIZE / 2; i--) {
//                primeNumbers[i] = isPrime(i);
//                if (primeNumbers[i]) {
//                    System.out.println(i + " ist eine Primzahl");
//                }
//            }
//        });

        Thread firstHalfGenerated = new Thread(firstHalfR, "First Half");
        Thread secondHalfGenerated = new Thread(secondHalfR, "Second Half");

        synchronized (Aufgabe_Loesung_1_PrimeNumberFinder.this) {
            firstHalfGenerated.start();
        }

        synchronized (Aufgabe_Loesung_1_PrimeNumberFinder.this) {
            secondHalfGenerated.start();
        }

        secondHalfGenerated.join();
        firstHalfGenerated.join();

        System.out.println(counter);

//        firstHalfT.start();
//        secondHalfT.start();
    }

    private boolean isPrime(int x) {

        if (x % 2 == 0) {
            return false;
        }

        for (int i = 3; i < x; i++) {
            if (x % i == 0) {
                return false;
            }
        }

        return true;
    }

    // Aufgabe01: Schreibe die Methode findPrimeNumbers() so um, dass sie zwei Threads verwendet.
    // Jeder Thread soll die Hälfte der Zahlen prüfen, ob sie Primzahlen sind.
}
