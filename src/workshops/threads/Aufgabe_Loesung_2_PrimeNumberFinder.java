package workshops.threads;

public class Aufgabe_Loesung_2_PrimeNumberFinder {

    private static final int SIZE = 10_000;

    private static int counter = 0;

    private boolean[] primeNumbers;


    public static void main(String[] args) {
        new Aufgabe_Loesung_2_PrimeNumberFinder().findPrimeNumbers();
    }

    public Aufgabe_Loesung_2_PrimeNumberFinder() {
        this.primeNumbers = new boolean[SIZE];
    }

    private void findPrimeNumbers() {
        synchronized (Aufgabe_Loesung_2_PrimeNumberFinder.this) {
            new PrimeNumberFinderThread(0, SIZE / 2).start();
        }
        synchronized (Aufgabe_Loesung_2_PrimeNumberFinder.this) {
            new PrimeNumberFinderThread(SIZE / 2, SIZE).start();
        }

        System.out.println(counter);
    }


    private class PrimeNumberFinderThread extends Thread {

        /**
         * Zahl einschließlich, ab der die Primzahlen berechnet werden sollen.
         */
        private int from;

        /**
         * Zahl ausschließlich, bis zu der die Primzahlen berechnet werden sollen.
         */
        private int to;


        public PrimeNumberFinderThread(int from, int to) {
            super("primeNumberFinder");
            this.from = from;
            this.to = to;
        }

        @Override
        public void run() {
            for (int i = from; i < to; i++) {
                primeNumbers[i] = isPrime(i);
                if (primeNumbers[i]) {
                    System.out.println(i + " ist eine Primzahl!");
                    counter++;
                }
            }
        }

        private boolean isPrime(int x) {

            if (x % 2 == 0) {
                return false;
            }

            for (int i = 3; i < x; i++) {
                if (x % i == 0) {
                    return false;
                }
            }

            return true;
        }
    }
}


// Aufgabe02: Schreibe die Klasse A08_PrimeNumberFinder_Loesung01 so um, dass ein Zähler für beide Threads gemeinsam mitzählt, wie viele Primzahlen gefunden wurden.
// Denke daran den Zugriff auf Deinen Zähler zu synchronisieren, damit er korrekt zählt.
// Tipp: Es gibt 1229 Primzahlen zwischen 0 und 9999
