package sample.streams;

import java.util.*;
import java.util.stream.Collectors;

public class Streams {

    private static List<Person> personList = new ArrayList<>();
    private static List<Person> humanList = new ArrayList<>();
    private static List metaList = new ArrayList();
    private static Set<Person> personSet = new HashSet<>();

    public static void main(String[] args) {

        personList.add(new Person("Max", Gender.MALE, 20));
        personList.add(new Person("Moriz", Gender.MALE, 25));
        personList.add(new Person("Lisa", Gender.FEMALE, 22));
        personList.add(new Person("Anna", Gender.FEMALE, 19));
        personList.add(new Person("Petra", Gender.FEMALE, 28));
        metaList.add(personList);

        humanList.add(new Person("Micha", Gender.MALE, 20));
        humanList.add(new Person("Marko", Gender.MALE, 25));
        humanList.add(new Person("Leyla", Gender.FEMALE, 22));
        humanList.add(new Person("Pjotr", Gender.FEMALE, 19));
        humanList.add(new Person("Viktor", Gender.FEMALE, 28));
        metaList.add(humanList);

        personSet.add(new Person("SetPerson1", Gender.FEMALE, 35));
        personSet.add(new Person("SetPerson2", Gender.MALE, 25));
        personSet.add(new Person("SetPerson3", Gender.MALE, 30));

        System.out.println("Method Reference");
        personSet.forEach(Person::printName);

        personSet.forEach(Person::getName);

        System.out.println("Lambda");
        personSet.stream()
                .forEach(p -> {
                    System.out.println(p.getName());
                });

        List<Person> ageList = personList.stream()
                .filter(person -> person.getAge() >= 20)
                .collect(Collectors.toList());

        System.out.println(ageList.get(0).getName());

        List<Person> neu = personList.stream()
                .filter(person -> person.getAge() >= 25)
                .map(e -> new Person(e.getName(), e.getGender(), e.getAge()))
                .collect(Collectors.toList());

        neu.forEach(e -> System.out.println(e.getName().toUpperCase()));

        HashSet<Person> neu1 = personList.stream()
                .filter(person -> person.getAge() >= 25)
                .map(e -> new Person(e.getName().toUpperCase(), e.getGender(), e.getAge()))
                .collect(Collectors.toCollection(HashSet::new));
        neu1.forEach(e -> System.out.println(e.getName()));


        List<Person> liste = personList.stream()
                .filter(person -> person.getGender().equals(Gender.FEMALE))
                .sorted(Comparator.comparing(Person::getName))
                .map(person -> new Person(person.getName(), person.getGender(), person.getAge()))
                .collect(Collectors.toList());

        liste.forEach(e -> System.out.println(e.getName()));


        Optional<Person> first = personList.stream()
                .sorted(Comparator.comparing(Person::getName))
                .findFirst();

        first.ifPresent(p -> System.out.println(p.getName() + " " + p.getAge()));

        System.out.println();

        personList.stream()
                .max(Comparator.comparing(Person::getName))
                .ifPresent(e -> System.out.println(e.getName() + " " + e.getAge()));

        System.out.println();

        personList.stream()
                .sorted(Comparator.comparing(Person::getGender).reversed().thenComparing(Person::getAge).reversed())
                .forEach(p -> System.out.println(p.getName() + ": " + "\n" + p.getAge() + "\t" + p.getGender()));

        System.out.println();

        Map<Gender, List<Person>> group = personList.stream()
                .collect(Collectors.groupingBy(Person::getGender));

        group.forEach((gender, people) -> {
            System.out.println(gender);
            people.forEach(p -> System.out.println(p.getName() + " " + p.getAge()));
            System.out.println();
        });

        System.out.println("Last: ");

        Map<String, List<Person>> newGroup = personList.stream()
                .sorted(Comparator.comparing(Person::getName))
                .collect(Collectors.groupingBy(Person::getName));

        newGroup.forEach((name, people) -> {
            System.out.println(name);
            people.forEach(p -> System.out.println(p.getAge() + " " + p.getGender()));
            System.out.println();
        });

        System.out.println("Last1: ");

        Map<Gender, List<Person>> newGroup1 = personList.stream()
                .sorted(Comparator.comparing(Person::getAge))
                .collect(Collectors.groupingBy(Person::getGender));

        newGroup1.forEach((gender, people) -> {
            System.out.println(gender);
            people.forEach(p -> System.out.println(p.getName() + " " + p.getAge()));
            System.out.println();
        });


        Map<Gender, List<Person>> newGroup2 = personList.stream()
                .collect(Collectors.groupingBy(Person::getGender));

        Map<Gender, List<Person>> liste3 = new HashMap<>();

        liste3 = personList.stream()
                .collect(Collectors.groupingBy(Person::getGender));


        Map<Gender, List<Person>> test = personList.stream()
                .filter(p -> p.getGender().equals(Gender.FEMALE))
                .collect(Collectors.groupingBy(Person::getGender));

        test.forEach((gender, people) -> {
                    System.out.println(gender + ": ");
                    people.forEach(p -> System.out.println(p.getName()));
                }
        );

        Set<Person> neuSet = personList.stream()
                .filter(p -> p.getAge() < 20)
                .collect(Collectors.toSet());

        System.out.println("Personen unter 20: ");
        neuSet.forEach(p -> System.out.println(p.getName() + " " + p.getAge()));


        List<Person> neueListe = personList.stream()
                .map(p -> new Person(p.getName().toUpperCase(), p.getGender(), p.getAge()))
                .sorted(Comparator.comparing(Person::getAge))
                .collect(Collectors.toList());

        neueListe.forEach(p -> System.out.println(p.getName() + " " + p.getAge()));

        neueListe.forEach(item -> System.out.println(item.getName().length()));

        personList.stream()
                .limit(3)
                .forEach(p -> System.out.println(p.getName()));

        Integer sum = personList.stream()
                .map(Person::getAge)
                .reduce(0, Integer::sum);
        System.out.println(sum);

        Optional<String> teamsTest = personList.stream()
                .map(Person::getName)
                .reduce((a, b) -> a + " und " + b);
        System.out.println(teamsTest.get());


        Integer produkt = personList.stream()
                .map(Person::getAge)
                .reduce(0, (a, b) -> a * b);
        System.out.println(produkt);

        personList.stream()
                .mapToInt(Person::getAge)
                .average()
                .ifPresent(avg -> System.out.println("Average: " + avg));

        IntSummaryStatistics stats = personList.stream()
                .mapToInt(Person::getAge)
                .summaryStatistics();
        System.out.println(stats);

        personList.stream()
                .map(p -> p.getName().length())
                .forEach(System.out::println);

    }
}
