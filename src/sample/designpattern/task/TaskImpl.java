package sample.designpattern.task;


import sample.designpattern.interfaces.Task;
import sample.designpattern.interfaces.TaskObserver;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class TaskImpl implements Task {

    private String task;
    private List<TaskObserver> meineObserver = new ArrayList<>();

    public TaskImpl(String task) {
        this.task = task;
    }

    @Override
    public String getName() {
        return task;
    }

    @Override
    public void perform(PrintStream out) {
        out.println(" " +this.getName());
        if(!meineObserver.isEmpty()) {
            meineObserver.forEach(p -> p.onComplete());
        }
    }

    @Override
    public void addObserver(TaskObserver taskObserver) {
        meineObserver.add(taskObserver);
    }
}
