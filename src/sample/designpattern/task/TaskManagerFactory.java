package sample.designpattern.task;


import sample.designpattern.interfaces.TaskManager;

import java.util.ArrayList;

public class TaskManagerFactory {

    public static TaskManager createTaskManager() {
        return new ListTaskManager(new ArrayList());
    };

}
