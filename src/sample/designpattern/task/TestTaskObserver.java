package sample.designpattern.task;


import sample.designpattern.interfaces.TaskObserver;

public class TestTaskObserver implements TaskObserver {
    @Override
    public void onComplete() {
        System.out.println("TestTaskObserver onComplete");
    }
}
