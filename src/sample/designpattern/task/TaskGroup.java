package sample.designpattern.task;


import sample.designpattern.interfaces.Task;
import sample.designpattern.interfaces.TaskObserver;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class TaskGroup implements Task {

    private String taskGroupName;
    private List<Task> taskGroup = new ArrayList<>();

    private List<TaskObserver> meineObserver = new ArrayList<>();

    public TaskGroup(String taskGroupName) {
        this.taskGroupName = taskGroupName;
    }

    public void addSubTask(Task subtask) {
        taskGroup.add(subtask);
    }

    @Override
    public String getName() {
        return taskGroupName;
    }

    @Override
    public void perform(PrintStream out) {
        out.println(taskGroupName);
        taskGroup.forEach( task -> {
            System.out.print("  ");
            task.perform(out);
        });
        if(!meineObserver.isEmpty()) {
            meineObserver.forEach(p -> p.onComplete());
        }
    }

    @Override
    public void addObserver(TaskObserver taskObserver) {
        meineObserver.add(taskObserver);
    }
}
