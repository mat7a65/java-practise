package sample.designpattern.task;


import sample.designpattern.interfaces.Task;
import sample.designpattern.interfaces.TaskObserver;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class ImportantTask implements Task {

    private Task importantTask;
    private List<TaskObserver> meineObserver = new ArrayList<>();

    public ImportantTask(Task task) {
        this.importantTask = task;
    }

    @Override
    public String getName() {
        return importantTask.getName();
    }

    @Override
    public void perform(PrintStream out) {
        out.println("ACHTUNG, das ist jetzt wichtig:");
        out.println(this.getName());

        if(!meineObserver.isEmpty()) {
            meineObserver.forEach(p -> p.onComplete());
        }
    }

    @Override
    public void addObserver(TaskObserver taskObserver) {
        meineObserver.add(taskObserver);
    }

}
