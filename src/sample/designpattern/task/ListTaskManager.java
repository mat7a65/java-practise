package sample.designpattern.task;


import sample.designpattern.interfaces.Task;
import sample.designpattern.interfaces.TaskManager;

import java.util.List;

public class ListTaskManager implements TaskManager {

    private List<Task> taskList;

    public ListTaskManager(List tasks) {
        this.taskList = tasks;
    }

    @Override
    public void addTask(Task task) {
        this.taskList.add(task);
    }

    @Override
    public void performTasks() {
        taskList.forEach(task -> {
            task.perform(System.out);
        });

//        for (Task task : taskList){
//            task.perform(System.out);
//        }
//
//        this.taskList.stream()
//                .forEach(p -> System.out.println(p.getName()));
    }
}
