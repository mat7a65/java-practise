package sample.designpattern.interfaces;

public interface TaskManager {
    void addTask(Task task);

    void performTasks();
}
