package sample.designpattern.interfaces;

public interface TaskObserver {
    void onComplete();
}
