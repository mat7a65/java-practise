package sample.designpattern;


import sample.designpattern.interfaces.Task;
import sample.designpattern.interfaces.TaskManager;
import sample.designpattern.task.*;

public class Main {
    public static void main(String[] args) {

//      Durch Factory ersetzt:
//      TaskManager manager = new ListTaskManager(new ArrayList());
        TaskManager manager = TaskManagerFactory.createTaskManager();

        manager.addTask(new TaskImpl("Wäsche waschen"));
        manager.addTask(new TaskImpl("Küche putzen"));

        manager.performTasks();

        TaskGroup einkaufen = new TaskGroup("Einkaufen");

        TaskGroup bäcker = new TaskGroup("Bäcker");
        bäcker.addSubTask(new TaskImpl("Brezen"));
        bäcker.addSubTask(new TaskImpl("Semmeln"));
        einkaufen.addSubTask(bäcker);

        TaskGroup supermarkt = new TaskGroup("Supermarkt");

        TaskGroup gemüse = new TaskGroup("Gemüse");
        gemüse.addSubTask(new TaskImpl("Gurke"));
        gemüse.addSubTask(new TaskImpl("Tomaten"));
        supermarkt.addSubTask(gemüse);

        TaskGroup kühlschrank = new TaskGroup("Kühlschrank");
        kühlschrank.addSubTask(new TaskImpl("Butter"));
        supermarkt.addSubTask(kühlschrank);
        einkaufen.addSubTask(supermarkt);

        TaskGroup putzen = new TaskGroup("Putzen");

        TaskGroup badezimmer = new TaskGroup("Badezimmer");
        badezimmer.addSubTask(new TaskImpl("Badewanne"));
        badezimmer.addSubTask(new TaskImpl("Waschbecken"));
        putzen.addSubTask(badezimmer);

        TaskGroup küche = new TaskGroup("Küche");
        küche.addSubTask(new TaskImpl("Waschbecken"));
        küche.addSubTask(new TaskImpl("Tisch"));
        putzen.addSubTask(küche);

        einkaufen.perform(System.out);
        putzen.perform(System.out);

        Task zähnePutzen = new TaskImpl("Zähne putzen");
        manager.addTask(zähnePutzen);

        Task essenEinpacken = new TaskImpl("Essen einpacken");
        Task essenEinpackenWichtig = new ImportantTask(essenEinpacken);
        manager.addTask(essenEinpackenWichtig);

        manager.addTask(new TaskImpl("Schuhe binden"));

        zähnePutzen.addObserver(new TestTaskObserver());
        essenEinpackenWichtig.addObserver(() -> System.out.println("Essen eingepackt, Lambda aufgerufen"));

        manager.performTasks();
    }
}
