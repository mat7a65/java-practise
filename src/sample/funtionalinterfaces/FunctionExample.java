package sample.funtionalinterfaces;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FunctionExample {

    public static void main(String[] args) {

        Customer customer1 = new Customer(1, "Hans Meier", 45);
        Customer customer2 = new Customer(2, "Anna Müller", 50);
        Customer customer3 = new Customer(4, "Bruce Wayne", 35);
        Customer customer4 = new Customer(5, "Peter Parker", 30);

        List<Customer> customerList = new ArrayList<>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        customerList.add(customer4);

        Predicate<Customer> customerPredicate = customer -> customer.getAge() < 40;
        List<String> liste =
                customerList.stream()
                        .filter(customerPredicate)
                        .map(Customer::getCustomerName)
                        .collect(Collectors.toList());

        liste.forEach(System.out::println);

        Function<Customer, String> customerNameFunction = Customer::getCustomerName;

        customerList.stream()
                .filter(customerPredicate)
                .map(customerNameFunction)
                .forEach(element -> System.out.println(element.toUpperCase()));


        Function<Customer, HashMap.Entry<Integer, String>> newFunction =
                customer -> new AbstractMap.SimpleEntry<>(customer.getCustomerId(), customer.getCustomerName());

        customerList.stream()
                .filter(customerPredicate)
                .map(newFunction)
                .forEach(x -> {
                    System.out.println("Key: " + x.getKey());
                    System.out.println("Value: "+ x.getValue());
                });

    }
}
