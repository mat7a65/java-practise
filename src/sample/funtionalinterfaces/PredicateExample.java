package sample.funtionalinterfaces;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collector;

public class PredicateExample {

    public static void main(String[] args) {

        Customer customer1 = new Customer(1, "Hans Meier", 45);
        Customer customer2 = new Customer(2, "Anna Müller", 50);
        Customer customer3 = new Customer(4, "Bruce Wayne", 35);
        Customer customer4 = new Customer(5, "Peter Parker", 30);

        List<Customer> customerList = new ArrayList<>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        customerList.add(customer4);

        Predicate<Customer> customerPredicate = p -> p.getAge() > 31;
        Consumer<Customer> customerConsumer = c -> System.out.println(c.getCustomerName());

        customerList.stream()
                .filter(customerPredicate)
                .forEach(customerConsumer);

//        Returns true if any element of the list matches the predicates requirements
        Boolean anyMatch = customerList.stream().anyMatch(customerPredicate);
        System.out.println(anyMatch);

//        Returns true if all elements match the predicates requirements
        Boolean allMatch = customerList.stream().allMatch(customerPredicate);
        System.out.println(allMatch);

//        Takes the elements not matching the predicates requirements and return them as a Stream <Customer>
        customerList.stream()
                .dropWhile(customerPredicate)
                .forEach(customer -> System.out.println(customer.toString()));
    }
}
