package sample.funtionalinterfaces;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ConsumerExample {

    public static void main(String[] args) {

//        Integer-Consumer
        Consumer<Integer> integerConsumer = c -> System.out.println("This is: " + c);
        List<Integer> integerList = Arrays.asList(1, 2, 3, 4, 5, 6);
        integerList.forEach(integerConsumer);

//        String Consumer BiConsumer
        List<String> stringList = new ArrayList<>();
        Map<Integer, String> integerStringMap = new HashMap<>();
        integerStringMap.put(1, "Otto Omega");
        integerStringMap.put(2, "Paul Panther");
        integerStringMap.put(3, "Donald Duck");

        BiConsumer<Integer, String> biConsumer = (i, t) -> stringList.add(t);
        Consumer<String> stringConsumer = System.out::println;

        integerStringMap.forEach(biConsumer);
        stringList.forEach(stringConsumer);

//        Consumer Example
        Customer customer1 = new Customer(1, "Hans Meier", 45);
        Customer customer2 = new Customer(2, "Anna Müller", 50);
        Customer customer3 = new Customer(4, "Bruce Wayne", 35);
        Customer customer4 = new Customer(5, "Peter Parker", 30);

        List<Customer> customerList = new ArrayList<>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        customerList.add(customer4);

        customerList.forEach(customer -> System.out.println(customer.toString()));
    }
}
