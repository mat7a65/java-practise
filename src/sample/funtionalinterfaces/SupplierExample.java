package sample.funtionalinterfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class SupplierExample {

    public static void main(String[] args) {
        Customer customer1 = new Customer(1, "Hans Meier", 45);
        Customer customer2 = new Customer(2, "Anna Müller", 50);
        Customer customer3 = new Customer(4, "Bruce Wayne", 35);
        Customer customer4 = new Customer(5, "Peter Parker", 30);

        List<Customer> customerList = new ArrayList<>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        customerList.add(customer4);

//        Supplier-Method
        Supplier<String> stringSupplier = () -> "Not found, dude!";
        List<String> stringList = List.of("Hallo", "Ballo");
        System.out.println(
                stringList.stream()
                        .filter(p -> p.length() < 1)
                        .findAny().orElseGet(stringSupplier));

//       Customer-Supplier
        Supplier<Customer> customerSupplier = () -> new Customer(0, "Dummy", 0);
        System.out.println(
                customerList.stream()
                        .filter(c -> c.getAge() > 55)
                        .findFirst()
                        .orElseGet(customerSupplier)
        );
    }
}
