package sample.funtionalinterfaces;

import java.lang.reflect.Array;
import java.util.*;

public class ComparatorExample {

    public static void main(String[] args) {
        Customer customer1 = new Customer(1, "Hans Meier", 45);
        Customer customer2 = new Customer(2, "Anna Müller", 50);
        Customer customer3 = new Customer(4, "Bruce Wayne", 35);
        Customer customer4 = new Customer(5, "Peter Parker", 30);
        Customer customer5 = new Customer(5, "Puter Past", 30);

        List<Customer> customerList = new ArrayList<>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        customerList.add(customer4);
        customerList.add(customer5);

        Comparator<Customer> comp = (s1, s2) -> {
            if (s1.getCustomerName().equals(s2.getCustomerName())) {
                return 0;
            } else if (s1.getCustomerName().charAt(0) > s2.getCustomerName().charAt(0)) {
                return -1;
            }
            return 1;
        };

        customerList.stream()
                .sorted(comp)
                .forEach(System.out::println);


        Room room1 = new Room("Bad", 2, 4);
        Room room2 = new Room("Küche", 2, 3);
        Room room3 = new Room("Wohnzimmer", 7, 8);

        List<Room> roomList = Arrays.asList(room1, room2, room3);

        Comparator<Room> sizeComp = (r1, r2) -> {
            if ((r1.length * r1.width) == (r2.length * r2.width)) {
                return 0;
            } else if ((r1.length * r1.width) < (r2.length * r2.width)) {
                return 1;
            }
            return -1;
        };

        roomList.stream()
                .sorted(sizeComp)
                .forEach(room -> {
                    System.out.println(room.name);
                });


        List<Integer> integerList = Arrays.asList(2, 5, 6, 8, 3, 7, 55, 125, 80);
        integerList.stream()
                .sorted()
                .forEach(System.out::println);
    }
}
