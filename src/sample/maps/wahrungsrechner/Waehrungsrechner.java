package sample.maps.wahrungsrechner;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Waehrungsrechner {
    public static void main(String[] args) {

        String zeichen = "Hello World!";

        HashSet<Character> test = new HashSet<>();
        Map<Character, Integer> countMap = new HashMap<>();

        for(Character c : zeichen.toCharArray()) {
            test.add(c);
        }
        System.out.println();

        for (Character c : test) {
            System.out.print(c + " ");
            int counter = 0;
            for(int i = 0; i<= zeichen.length()-1; i++) {
                if (c.equals(zeichen.charAt(i))) {
                    counter++;
                    System.out.print('*');
                }
                countMap.put(c, counter);
            }
            System.out.println();
        }
        System.out.println();

        ermittleMax(countMap);

        System.out.println();


        HashMap<String, Integer> map = new HashMap<>();
        map.put("USD", 117);
        map.put("GBP", 88);
        map.put("DKK", 746);
        map.put("SEK", 1034);

        double ergebnis = dollar(1, map);
        System.out.println("1 Dollar kostet " + round(ergebnis, 2) + " Euro");


        if (!map.containsKey("PLN")) {
            map.put("PLN", 429);
        }

        gebeAus(map);
        druckeRate(map);
        gebeAlleAus(map);

        map.put("PLN", 422);

        System.out.println("Der neue Wechselkurs für PLN beträgt " + map.get("PLN"));


    }

    public static void ermittleMax(Map<Character, Integer> counterMap) {
        int temp = 0;
        char sign = 'c';

        for(HashMap.Entry<Character, Integer> max : counterMap.entrySet()) {
            if(max.getValue() >= temp) {
                temp = max.getValue();
                sign = max.getKey();
            }
        }
        System.out.println("Das häufigste Zeichen ist '" + sign + "' und kommt " + temp + " Mal vor.");


    }

    public static double round(double value, int decimalPoints) {
        double d = Math.pow(10, decimalPoints);
        return Math.round(value*d)/d;
    }

    public static double dollar(Integer value, HashMap<String, Integer> map) {

        double kurs = map.get("USD");
        return (value/kurs)*100;
    };

    public static void gebeAus(HashMap<String, Integer> map) {
        System.out.println("Alle Währungen: " + map.keySet());
    }

    public static void druckeRate(HashMap<String, Integer> map) {
        System.out.println("Alle Preise: " + map.values());
    }

    public static void gebeAlleAus(HashMap<String, Integer> map) {
        System.out.println("Alle Währungen und Preise: " + map.entrySet());
    }


}
