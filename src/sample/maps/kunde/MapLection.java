package sample.maps.kunde;

import sample.maps.kunde.Kunde;

import java.util.*;

public class MapLection {

    public static HashMap<Integer, Kunde> kundenMap = new HashMap<Integer, Kunde>();

    public static void main(String[] args) {

        kundenMap.put(1, new Kunde("Hans"));
        kundenMap.put(2, new Kunde("Otto"));
        kundenMap.put(3, new Kunde("Flotto"));

        Set<Map.Entry<Integer, Kunde>> kundenSet = kundenMap.entrySet();
        kundenSet.forEach(p -> System.out.println("Key: " + p.getKey()+ " Value: " + p.getValue().getName()));

        Kunde testKunde = kundenMap.get(1);
        System.out.println(testKunde.getName());

        Collection<Kunde> values = kundenMap.values();

        values.stream()
                .forEach(p -> System.out.println(p.getName()));

        kundenMap.values().stream()
                .forEach(p -> System.out.println(p.getName().toUpperCase()));

        for(Kunde value : kundenMap.values()) {
            System.out.println(value.getName().toLowerCase());
        }

        Collection<Kunde> versuch = new ArrayList<>();
        versuch = kundenMap.values();

        versuch.stream()
                .forEach(p -> System.out.println(p.getName()));

    }
}
