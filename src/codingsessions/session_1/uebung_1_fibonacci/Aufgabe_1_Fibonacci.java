package codingsessions.session_1.uebung_1_fibonacci;

public class Aufgabe_1_Fibonacci {

    public static void main(String[] args) {
        int ergebnis = berechneFibonacci(10);
        System.out.println(ergebnis);
    }


    public static int berechneFibonacci(int i) {
        if (i == 1) {
            return 0;
        } else if (i == 2) {
            return 1;
        } else {
            return berechneFibonacci(i - 1) + berechneFibonacci(i - 2);
        }
    }


    // Die Fibonaccifolge beginnt mit den Zahlen 0 und 1. Alle Weiteren Elemente der Folge werden durch Addition der beiden vorherigen Folgen-Glieder bestimmt:
    // 0, 1, 1, 2, 3, 5, ...
    // Mathematisch f(1) = 0, f(2) = 1, f(n) = f(n-1) + f(n-2)

    // Aufgabe 1: Schreibe eine rekursive Methode, die die Fibonacci-Folge berechnet. Berechne damit die 10. Zahl der Folge.
}

