package codingsessions.session_1.uebung_1_fibonacci;

public class Loesung_1_Fibonacci {
    public static void main(String[] args) {
        System.out.println(new Loesung_1_Fibonacci().fibonacci(10));
    }

    private int fibonacci(int i) {
        if (i == 1) {
            return 0;
        } else if (i == 2) {
            return 1;
        } else {
            return fibonacci(i - 1) + fibonacci(i - 2);
        }
    }
}
