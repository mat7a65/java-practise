package codingsessions.session_1.uebung_2_sudoku;

public class SudokuField {

    /** Maximale Feld-Nummer. */
    private static final int LAST_FIELD_NO = 80;


    /** Laufende Nummer des Feldes (0 bis 80). */
    private int fieldNo;

    /** Zeilennummer des Feldes (0 bis 8). */
    private int row;

    /** Spaltennummer des Feldes (0 bis 8). */
    private int col;


    /**
     * Liefert eine Instanz für das erste Sudoku-Feld.
     *
     * @return Sudoku-Feld in Zeile 0 und Spalte 0.
     */
    public static SudokuField getFirst() {
        return new SudokuField(0);
    }

    /**
     * Privater Konstruktor zur Erzeugung eines Sudoku-Feldes.
     *
     * @param fieldNo Laufende Nummer des Feldes (0 bis 80).
     */
    private SudokuField(int fieldNo) {
        this.fieldNo = fieldNo;
        this.row = fieldNo / 9;
        this.col = fieldNo % 9;
    }

    /**
     * Liefert ein Flag, ob es noch ein weiteres Sudoku-Feld gibt.
     *
     * @return <code>true</code>, wenn es ein weiteres Sudoku-Feld gibt, <code>false</code> sonst.
     */
    public boolean hasNext() {
        return fieldNo < LAST_FIELD_NO;
    }

    /**
     * Liefert das nächste Sudoku-Feld.
     *
     * @return <code>null</code>, wenn es kein weiteres Feld gibt.
     */
    public SudokuField next() {
        if (fieldNo == LAST_FIELD_NO) {
            return null;
        }
        return new SudokuField(fieldNo + 1);
    }

    /**
     * Liefert die Zeilennummer, dieses Feldes.
     *
     * @return Zeilennummer im Wertebereich 0 bis 8.
     */
    public int getRow() {
        return row;
    }

    /**
     * Liefert die Spaltennummer, dieses Feldes.
     *
     * @return Spaltennummer im Wertebereich 0 bis 8.
     */
    public int getCol() {
        return col;
    }
}
