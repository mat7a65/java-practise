package codingsessions.session_1.uebung_2_sudoku;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class SudokuView extends JFrame implements SudokuModel.ModelChangeListener {

    private static final String EMPTY_FIELD_VALUE = "  ";


    private SudokuModel sudokuModel;

    private JTextField[][] textFields = new JTextField[9][9];

    public SudokuView(SudokuModel sudokuModel) throws HeadlessException {
        super("SudokuSolver");

        this.sudokuModel = sudokuModel;
        sudokuModel.addChangeListener(this);

        createSudokuGrid();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                System.exit(0);
            }
        });

        setSize(650, 550);
        setVisible(true);
    }

    @Override
    public void modelChanged(int row, int col, Integer value) {
        String textValue = (value == null ? EMPTY_FIELD_VALUE : value.toString());
        textFields[row][col].setText(textValue);
    }

    private void createSudokuGrid() {
        GridLayout gridLayout = new GridLayout(3, 3);
        getContentPane().setLayout(gridLayout);
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                JPanel panel = create3x3Panel(row * 3, col * 3);
                panel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
                add(panel);
            }
        }
    }

    private JPanel create3x3Panel(int rowOffset, int colOffset) {
        JPanel panel3x3 = new JPanel();
        GridLayout gridLayout = new GridLayout(3, 3);
        panel3x3.setLayout(gridLayout);
        Font font = new JTextField().getFont();
        Font bigFont = new Font(font.getFamily(), Font.BOLD, 24);
        for (int row = rowOffset; row < rowOffset + 3; row++) {
            for (int col = colOffset; col < colOffset + 3; col++) {
                JPanel panel = new JPanel();
                panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                JTextField textField = new JTextField();
                String textValue = sudokuModel.getTextValue(row, col);
                if (textValue == null) {
                    textField.setText(EMPTY_FIELD_VALUE);
                    textField.setForeground(Color.BLACK);
                } else {
                    textField.setText(textValue);
                    textField.setForeground(Color.BLUE);
                }
                textField.setEditable(false);
                textField.setFont(bigFont);
                textField.setBorder(null);
                textFields[row][col] = textField;
                panel.add(textField);
                panel3x3.add(panel);
            }
        }
        return panel3x3;
    }
}
