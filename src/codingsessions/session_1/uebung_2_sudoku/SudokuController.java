package codingsessions.session_1.uebung_2_sudoku;

import java.awt.*;
import java.util.Scanner;

public class SudokuController {

    /**
     * Ausführungs-Modus: Schnell, langsam, schrittweise.
     */
    private ExecMode execMode = ExecMode.FAST;


    /**
     * Ausgangszustand des Sudokus. x steht für ein leeres Feld.
     */
    private static final String[] SUDOKU = {
            "xx7xx1xx2",
            "x93xx2x7x",
            "x2x8x59xx",
            "x4x78xx1x",
            "xxxxxxxxx",
            "x8xx39x2x",
            "xx19x8x5x",
            "x3x2xx68x",
            "2xx4xx1xx"
    };


    /**
     * Model für das Sudoku.
     */
    private SudokuModel sudokuModel;

    /**
     * View für Sudoku.
     */
    private SudokuView sudokuView;

    /**
     * <code>true</code>, wenn eine Lösung für das Rätsel gefunden wurde.
     */
    private boolean solved;

    public static void main(String[] args) {
        new SudokuController().run();
    }

    private void run() {
        this.sudokuModel = new SudokuModel(SUDOKU);
        this.sudokuView = new SudokuView(sudokuModel);

        // Löse das Sudoku-Rätsel. Beginne damit, das erste Feld zu belegen.
        solveSudoku(SudokuField.getFirst());
    }

    /**
     * Rekursive-Methode, die den Backtracking-Algorithmus implementiert.
     *
     * @param field Sudoku-Feld, welches in diesem Methoden-Aufruf mit einem Wert belegt werden soll.
     */
    private void solveSudoku(SudokuField field) {

        // Hier muss der Backtracking-Algorithmus implementiert werden.
        // Folgende Schritte werden in passender Reihenfolge benötigt:

        // Prüfen, ob das Sudoku schon vollständig gelöst ist -> ok
        if (field == null) {
            System.out.println("Solved!");
            Toolkit.getDefaultToolkit().beep();
            this.solved = true;
            return;
        }

        int row = field.getRow();
        int col = field.getCol();

        // Prüfen, ob das aktuelle Feld schon gefüllt ist -> ok
        if (sudokuModel.hasValue(row, col)) {
            // Rekursiver Aufruf dieser Methode, um das nächste Feld zu belegen -> ok
            solveSudoku(field.next());
        } else {
            // Durchprobieren aller Werte für das aktuelle Feld -> ok
            for (int value = 1; value <= 9; value++) {
                boolean solveable = sudokuModel.setValue(row, col, value);
                slowDownExecution();
                if (solveable) {
                    solveSudoku(field.next());
                    if (solved) {
                        break;
                    }
                    slowDownExecution();
                }
                // Erkennung einer Sackegasse und zurücknehmen des letzten Wertes (Backtracking) -> ok
                sudokuModel.revertLastChange();
            }
        }
    }
    // Tipp: Verwende slowDownExecution() an Stellen, wo Du die Ausführung bremsen möchtest, um Deinen Algorithmus visuell zu testen!

    private void slowDownExecution() {
        switch (execMode) {
            case FAST:
                break;

            case SLOW:
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;

            case STEP:
                new Scanner(System.in).nextLine();
                break;
        }
    }

    public enum ExecMode {

        /**
         * Läuft so schnell wie möglich.
         */
        FAST,

        /**
         * Bremst den Algorithmus, um ihn besser beobachten zu können.
         */
        SLOW,

        /**
         * Wartet in der Standard-Konsole auf Drücken von Enter, um den Ablauf besser verfolgen zu können.
         */
        STEP;
    }
}

