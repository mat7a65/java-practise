package codingsessions.session_1.uebung_2_sudoku;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class SudokuModel {

    /** Dieses Zeichen repräsentiert bei der Initialisierung im Konstruktor ein leeres Sudoku-Feld. */
    private static final char EMPTY_CHAR = 'x';


    /** Inhalte der Sudoku-Felder (Werte 1 bis 9) bzw. <code>null</code>, wenn das Feld noch leer ist. */
    private Integer[][] values;

    /** Stapel der letzten Änderungen am Model. */
    private Stack<ModelChange> changes;

    /** Liste der Listener, die über Model-Änderungen informiert werden möchten. */
    private List<ModelChangeListener> changeListeners;


    /**
     * Initialisiert das Model aus einem String-Array.
     *
     * @param initialValues Jeder String enthält eine Zeile des vorgegebenen Rätsels.
     *                      Jedes Zeichen repräsentiert entweder eine vorgegebene Ziffer oder ein leeres Feld {@link #EMPTY_CHAR}.
     */
    public SudokuModel(String[] initialValues) {
        this.values = new Integer[9][9];
        this.changes = new Stack<>();
        this.changeListeners = new ArrayList<>();
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                char c = initialValues[row].charAt(col);
                Integer value = (c == EMPTY_CHAR) ? null : (c - '0');
                values[row][col] = value;
            }
        }
    }

    /**
     * Fügt einen Change-Listener hinzu, der bei Model-Änderungen benachrichtigt wird.
     *
     * @param changeListener Neuer Change-Listener.
     */
    public void addChangeListener(ModelChangeListener changeListener) {
        this.changeListeners.add(changeListener);
    }

    /**
     * Liefert den Inhalt eines Sudoku-Feldes.
     *
     * @param row Zeilennummer (0 bis 8).
     * @param col Spaltennummer (0 bis 8).
     * @return Inhalt des Feldes (Wert 1 bis 9) bzw. <code>null</code>, wenn das Feld leer ist.
     */
    public Integer getValue(int row, int col) {
        return values[row][col];
    }

    /**
     * Liefert den Inhalt eines Sudoku-Feldes als String.
     *
     * @param row Zeilennummer (0 bis 8).
     * @param col Spaltennummer (0 bis 8).
     * @return Inhalt des Feldes (Wert "1" bis "9") bzw. <code>null</code>, wenn das Feld leer ist.
     */
    public String getTextValue(int row, int col) {
        Integer value = getValue(row, col);
        return (value == null) ? null : value.toString();
    }

    /**
     * Prüft, ob ein Sudoku-Feld leer ist.
     *
     * @param row Zeilennummer (0 bis 8).
     * @param col Spaltennummer (0 bis 8).
     * @return <code>true</code>, wenn das Feld leer ist, <code>false</code>, wenn das Feld einen Wert enthält.
     */
    public boolean hasValue(int row, int col) {
        return getValue(row, col) != null;
    }

    /**
     * Setzt den Inhalt eines Sudoku-Feldes und liefert ein Flag, ob das Sudoku noch gültig ist.
     *
     * @param row Zeilennummer (0 bis 8).
     * @param col Spaltennummer (0 bis 8).
     * @param value Neuer Wert (1 bis 9) bzw. <code>null</code>, wenn das Feld leer sein soll.
     * @return <code>true</code>, wenn das Sudoku noch gültig ist. <code>false</code>, wenn der übergebene Wert dazu führt,
     * dass entweder in einer Zeile, einer Spalte oder einem Quadrat eine Zahl doppelt enthalten ist.
     */
    public boolean setValue(int row, int col, Integer value) {
        changes.add(new ModelChange(row, col, getValue(row, col)));
        this.values[row][col] = value;
        fireModelChangeEvents(row, col, value);
        if (value == null) {
            return true;
        } else {
            return checkField(row, col);
        }
    }

    /**
     * Nimmt die letzte Änderung am Model zurück und informiert alle Listener über die Änderung.
     */
    public void revertLastChange() {
        ModelChange lastChange = changes.pop();
        int row = lastChange.getRow();
        int col = lastChange.getCol();
        Integer oldValue = lastChange.getOldValue();
        this.values[row][col] = oldValue;
        fireModelChangeEvents(row, col, oldValue);
    }

    private void fireModelChangeEvents(int row, int col, Integer value) {
        for (ModelChangeListener changeListener : changeListeners) {
            changeListener.modelChanged(row, col, value);
        }
    }

    /**
     * Liefert ein Flag, ob das Sudoku an der übergebenen Position gültig ist.
     *
     * @param row Zeilennummer (0 bis 8).
     * @param col Spaltennummer (0 bis 8).
     * @return <code>true</code>, wenn das Sudoku gültig ist.
     * <code>false</code>, wenn an der übergebene Position entweder die Zeile, die Spalte oder das Quadrat eine Zahl
     * doppelt enthält.
     */
    private boolean checkField(int row, int col) {
        return checkRow(row) && checkCol(col) && checkSquare(row, col);
    }

    /**
     * Liefert ein Flag, ob das Sudoku in der übergebenen Zeile gültig ist.
     *
     * @param row Zeilennummer (0 bis 8).
     * @return <code>true</code>, wenn das Sudoku gültig ist.
     * <code>false</code>, wenn die übergebene Zeile eine Zahl doppelt enthält.
     */
    private boolean checkRow(int row) {
        boolean usedNumbers[] = new boolean[9];
        for (int col = 0; col < 9; col++) {
            Integer value = getValue(row, col);
            if (value != null) {
                if (usedNumbers[value - 1]) {
                    return false;
                }
                usedNumbers[value - 1] = true;
            }
        }
        return true;
    }

    /**
     * Liefert ein Flag, ob das Sudoku in der übergebenen Spalte gültig ist.
     *
     * @param col Spaltennummer (0 bis 8).
     * @return <code>true</code>, wenn das Sudoku gültig ist.
     * <code>false</code>, wenn die übergebene Spalte eine Zahl doppelt enthält.
     */
    private boolean checkCol(int col) {
        boolean usedNumbers[] = new boolean[9];
        for (int row = 0; row < 9; row++) {
            Integer value = getValue(row, col);
            if (value != null) {
                if (usedNumbers[value - 1]) {
                    return false;
                }
                usedNumbers[value - 1] = true;
            }
        }
        return true;
    }

    /**
     * Liefert ein Flag, ob das Sudoku in dem Quadrat gültig ist, welches zum übergebenen Feld gehört.
     *
     * @param rowToCheck Zeilennummer (0 bis 8).
     * @param colToCheck Spaltennummer (0 bis 8).
     * @return <code>true</code>, wenn das Sudoku gültig ist.
     * <code>false</code>, wenn das Quadrat des übergebenen Feldes eine Zahl doppelt enthält.
     */
    private boolean checkSquare(int rowToCheck, int colToCheck) {
        boolean usedNumbers[] = new boolean[9];

        int rowSquare = rowToCheck / 3;
        int minRow = rowSquare * 3;
        int maxRow = minRow + 2;

        int colSquare = colToCheck / 3;
        int minCol = colSquare * 3;
        int maxCol = minCol + 2;

        for (int row = minRow; row <= maxRow; row++) {
            for (int col = minCol; col <= maxCol; col++) {
                Integer value = getValue(row, col);
                if (value != null) {
                    if (usedNumbers[value- 1]) {
                        return false;
                    }
                    usedNumbers[value - 1] = true;
                }
            }
        }
        return true;
    }

    /**
     * Listener-Interface für Model-Änderungen.
     */
    public static interface ModelChangeListener {
        /**
         * Wird aufgerufen, wenn das Model geändert wurde.
         *
         * @param row Zeile des Feldes, dass geändert wurde.
         * @param col Spalte des Feldes, dass geändert wurde.
         * @param value Neuer Wert des Feldes.
         */
        public void modelChanged(int row, int col, Integer value);
    }

    /**
     * Container-Klasse für eine Änderung am Model.
     */
    private class ModelChange {
        /** Zeile des Feldes, dass geändert wurde. */
        private int row;

        /** Spalte des Feldes, dass geändert wurde. */
        private int col;

        /** Wert des Feldes, der überschrieben wurde bzw. <code>null</code>, wenn das Feld leer war. */
        private Integer oldValue;

        public ModelChange(int row, int col, Integer oldValue) {
            this.row = row;
            this.col = col;
            this.oldValue = oldValue;
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }

        public Integer getOldValue() {
            return oldValue;
        }
    }
}
