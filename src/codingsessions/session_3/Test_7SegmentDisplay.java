package codingsessions.session_3;

import org.junit.Test;

import static org.junit.Assert.*;

public class Test_7SegmentDisplay {

    Aufgabe_Loesung_7SegmentDisplay main = new Aufgabe_Loesung_7SegmentDisplay();

    @Test
    public void kommtVorZiffer0() {
        assertTrue(main.kommtVor(0, 'a'));
        assertTrue(main.kommtVor(0, 'b'));
        assertTrue(main.kommtVor(0, 'c'));
        assertFalse(main.kommtVor(0, 'd'));
        assertTrue(main.kommtVor(0, 'e'));
        assertTrue(main.kommtVor(0, 'f'));
        assertTrue(main.kommtVor(0, 'g'));
    }

    @Test
    public void welcheKommtVorZiffer1() {
        String test = main.welcheKommenVor(0);
        assertEquals("abcefg", test);
    }
}

