package codingsessions.session_3;

public class Aufgabe_Loesung_7SegmentDisplay {
    String[] segmente = {

//            0
            "abcefg",
//            1
            "cf",
//            2
            "acdeg",
//            3
            "acdfg",
//            4
            "bcdf",
//            5
            "abdfg",
//            6
            "abdefg",
//            7
            "acf",
//            8
            "abcdefg",
//            9
            "abcdfg"
    };



    public boolean kommtVor(int ziffer, char segment) {
        int position;
        position = segmente[ziffer].indexOf(segment);
        if(position >= 0) {
            return true;
        }
        else return false;

    }

    public String welcheKommenVor(int ziffer) {
        return segmente[ziffer];
    }
}

