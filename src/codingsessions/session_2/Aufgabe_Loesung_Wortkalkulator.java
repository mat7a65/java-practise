package codingsessions.session_2;

public class Aufgabe_Loesung_Wortkalkulator {
    private static int WORDS = 5;
    private static int DAYS = 23;

    public static void main(String[] args) {
        calcWithWords();
        calcWithDays();
    }

    private static void calcWithWords() {
        int day = 1;

        for (int i = 1; i < WORDS; i++) {
            day += i;
            System.out.println(i);
        }
        System.out.println("Anna can read " + WORDS + " words at day " + day);

    }

    private static void calcWithDays() {
        int day = DAYS;
        int count = 1;

        while(day > 0) {
            day -= count;
            count ++;
        }
        System.out.println("Anna can read " + (count-1) + " words at day " + DAYS);
    }

}
