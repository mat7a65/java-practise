package codingsessions.session_2;

import java.util.HashSet;
import java.util.Iterator;

public class Aufgabe_Loesung_Buchstabenraetsel {
    // Eingabeparameter
    private String summand1;
    private String summand2;
    private String summe;

    // UniqueCharacters
    private char[] uniqueChars;

    // IntArray figures
    private int[] figures;

    // boolean-Array 'verfügbare Zahlen'
    private boolean[] availableFigures = new boolean[10];




    public static void main(String[] args) {
        new Aufgabe_Loesung_Buchstabenraetsel("SEND", "MORE", "MONEY");
    }

    public Aufgabe_Loesung_Buchstabenraetsel(String summand1, String summand2, String summe) {
        this.summand1 = summand1;
        this.summand2 = summand2;
        this.summe = summe;
    }

    public void solve() {
        initUniqueCharacters();

        // 'figure'-Array auf die Länge des uniqueChar Arrays setzen. Figures ist int-Array
        figures = new int[uniqueChars.length];

        // setze die verfügbaren Zahlen auf 'true'
        for (int i = 0; i < availableFigures.length; i++) {
            availableFigures[i] = true;
        }
        permuteFigures(0);
    }


    public void initUniqueCharacters() {

        // HashSet instanziieren, um die 'gefundenen' Characters einzufügen
        HashSet<Character> foundChars = new HashSet<>();

        // einzelne Character werden je Element 'foundChars' hinzugefügt
        addUniqueCharactersToSet(foundChars, summand1);
        addUniqueCharactersToSet(foundChars, summand2);
        addUniqueCharactersToSet(foundChars, summe);

        // 'foundChars' enthält nun die einzelnen Elemente

        // setze die Größe des 'uniqueChar-Arrays' auf die Größe des 'foundChars-HashSet'
        uniqueChars = new char[foundChars.size()];

        // Iterator definieren
        Iterator<Character> charIter = foundChars.iterator();
        int i = 0;

        // uniqueChars befüllen mit den 'unique values' der Buchstaben -> figures
        while (charIter.hasNext()) {
            uniqueChars[i++] = charIter.next();
        }
    }

    public void addUniqueCharactersToSet(HashSet<Character> foundChars, String str) {
        // Durchlaufe den String, der in ein Char-Array umgewandelt wird und füge dem Set das 'c' hinzu
        for(char c: str.toCharArray() ) {
            foundChars.add(c);
        }
    }

    public void permuteFigures(int j) {
        for (int figure = 0; figure <= 9; figure++) {

        }
    }

}
